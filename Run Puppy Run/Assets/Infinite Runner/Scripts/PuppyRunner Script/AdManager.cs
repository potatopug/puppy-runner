﻿using UnityEngine;
using System.Collections;

using UnityEngine.Advertisements;
using InfiniteRunner.Game;

public class AdManager : MonoBehaviour
{
    static public AdManager instance;
    public bool IsVideoRevive = false;

    public void Start()
    {
        Debug.Log("IS INSTANCE!");
        instance = this;

    }

    public void ShowAd()
    {
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
    }

    private IEnumerator StartAnAd()
    {

        yield return new WaitForSeconds (5);
        ShowRewardedAd();
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
        else
            ShowAd();
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                if (IsVideoRevive)
                {
                   GameManager.instance.VideoAdRevive();
                   IsVideoRevive = false;
                }
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}

