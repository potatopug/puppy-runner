﻿using UnityEngine;
using System.Collections;
using InfiniteRunner.Game;
using InfiniteRunner.Player;


public class MovingCars : MonoBehaviour
{


    private float movementSpeed;
    private GameManager gameManager;
    private PlayerController playerController;
    private float distance;
    private bool canSpeedUp = true;
    private int allowSpeedUpTime = 5;
    void Start()
    {
        movementSpeed = Random.Range(2, 4);
        gameManager = GameManager.instance;
        playerController = PlayerController.instance;

    }

    // Update is called once per frame
    void Update()
    {

        if (playerController == null)
            playerController = PlayerController.instance;
        else
            distance = Vector3.Distance(transform.position, playerController.transform.position);

        if (gameManager.IsGameActive() && !gameManager.gamePaused && distance < 50)
            transform.position += transform.forward * Time.deltaTime * movementSpeed;


        //Debug.Log(string.Format("Distance between {0} and {1} is: {2}", transform, playerController, distance));
    }

    void FixedUpdate()
    {
        RaycastHit hit;
        Vector3 position_angle= (transform.forward + new Vector3(0,.5f,0)).normalized;
        Vector3 position_cast = new Vector3(transform.position.x, transform.position.y + .1f, transform.position.z);
        
        if (Physics.Raycast(position_cast, position_angle, out hit, 25)) 
        {
            Debug.DrawLine(position_cast, hit.point, Color.red);
            var script = hit.collider.gameObject.GetComponent<MovingCars>();
            if (script != null && canSpeedUp)
            {
                this.movementSpeed = script.movementSpeed  - .5f;
                script.movementSpeed += 1;
                canSpeedUp = false;
                StartCoroutine("LetSpeedUp");
            }
        }
    }

    private IEnumerator LetSpeedUp()
    {
        yield return new WaitForSeconds(allowSpeedUpTime);
        canSpeedUp = true;
    }
}