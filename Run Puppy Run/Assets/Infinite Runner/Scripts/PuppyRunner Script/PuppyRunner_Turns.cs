﻿using UnityEngine;
using System.Collections;
using VacuumShaders.CurvedWorld;

//this script allows us to create Turns bases on the shader on the fly.


public class PuppyRunner_Turns : MonoBehaviour {

    int baseX = 4;
    int baseY = 0;
    public float speed = .5f;







    void Start()
    {
     Invoke("LeftTurn",15);

       
    }



    void LeftTurn()
    {
        Vector3 thisturn = new Vector3(4, 8, 0);


        //StartCoroutine(TurnToo(thisturn));
   


    }



    IEnumerator TurnToo(Vector3 turnTo)
    {
        print("Here");
        Vector3 currentCurve = CurvedWorld_Controller.get.GetBend();
        Vector3 StartCurve = CurvedWorld_Controller.get.GetBend();
     
        float startTime = Time.time;
        float journeyLength = 3;

        while (currentCurve != turnTo)
        {
            print("And Here");

            float distCovered = (Time.time - startTime) * speed;
            float fracJourney = distCovered / journeyLength;
            currentCurve = Vector4.Lerp(StartCurve, turnTo, fracJourney);
            CurvedWorld_Controller.get.SetBend(currentCurve);
            currentCurve = CurvedWorld_Controller.get.GetBend();
            yield return new WaitForFixedUpdate();

        }
    }

   
}
