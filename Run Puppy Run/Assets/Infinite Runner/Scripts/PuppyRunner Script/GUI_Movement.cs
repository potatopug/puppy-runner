﻿using UnityEngine;
using System.Collections;
using InfiniteRunner.Game;


public class GUI_Movement : MonoBehaviour {
	

    //stupid easy - base it off panels.  Turn off a parent, turn on a Parent.

        //allow these to be arrays in case we want to have multiple panels on/off at once.

	private GUIManager guiManager;
    public GameObject[] turnOffMyParents;
    public GameObject[] turnOnTheirParents;
	public bool TurnOnStoreChar;
	public bool TurnOffStoreChar;

	public void Start()
	{
		guiManager = GUIManager.instance;

	}

    public void ChangePanelStates()
    {

        if (turnOffMyParents.Length > 0)
        {
            foreach (GameObject parent in turnOffMyParents)
            {
                parent.SetActive(false);
            }
        }
        if (turnOnTheirParents.Length > 0)
        {
            foreach (GameObject parent in turnOnTheirParents)
            {
                parent.SetActive(true);
            }
        }
		if (TurnOffStoreChar || TurnOnStoreChar) {	
			if (TurnOnStoreChar)
				guiManager.storeItemPreview.SetActive (true);
			else
				guiManager.storeItemPreview.SetActive (false);
		}
    }

}
